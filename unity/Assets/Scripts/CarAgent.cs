﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Vehicles.Car;

public class CarAgent : Agent {

	CarController controller;

	[SerializeField]
	GameObject target;

	[SerializeField]
	GameObject[] Gsensors;

    [SerializeField]
    GameObject generator;


	CarSensor[] sensors;

	Rigidbody rb;
	
	private float oldDistance;
	int solved=0,failed=0;

	const int accel=0, left=1, right=2, deccel=3;
	public override void CollectObservations ()
	{
		foreach (CarSensor s in sensors) {
			AddVectorObs (s.distance);	
		}

		AddVectorObs (controller.CurrentSpeed);
	}

	public override void AgentAction (float[] act,string name)
	{
		var reward = 0f;

		controller.Move(act[1],act[0],0,0);

		float distance= Vector3.Distance(transform.position,target.transform.position);


		if (oldDistance != float.PositiveInfinity && distance < oldDistance) {
			reward = 0.01f + (oldDistance - distance)/1000;
		}
		oldDistance=distance;

		
		
		var diffrenceEdge = Mathf.Abs(sensors[1].distance - sensors[2].distance);
		if(diffrenceEdge < 1.5) {
			reward += 0.01f;
		} else {
			reward -= diffrenceEdge/100;
		}
		Debug.Log("diffrence : "+diffrenceEdge);

        // check left and right sensor 
        for (var i = 1; i < 3;i++)
        {
            if(sensors[i].distance == -1) {
                reward = -1;
                Done();
                failed++;
                Debug.Log("Out of road");
            }
        }
		

		if (Vector3.Distance (transform.position, target.transform.position) < 1) {
			reward +=1f;
			Done();
			solved++;
		}

		SetReward(reward);

		Debug.Log("reward : "+reward);
	}


	public override void InitializeAgent ()
	{
		controller = gameObject.GetComponent<CarController> ();		
		sensors = new CarSensor[Gsensors.Length];

		for(var i = 0;i < Gsensors.Length;i++) {
			sensors [i] = Gsensors [i].GetComponent<CarSensor> ();
		}
		rb = GetComponent<Rigidbody> ();
	}

	public override void AgentReset ()
	{
		gameObject.transform.position = new Vector3 (0,0,2);
		gameObject.transform.rotation = new Quaternion(0,0,0,0);
		rb.velocity = Vector3.zero;
		oldDistance = float.PositiveInfinity;
		//target.transform.position = new Vector3(0,0,100*UnityEngine.Random.Range(100f,990f));
        generator.GetComponent<RoadGenerator>().generage();
	}

}
