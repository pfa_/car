﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarSensor : MonoBehaviour {

	public float distance;
    public string log;

	void Start () {
		
	}
	
	void Update () {
		RaycastHit hit;

		if ( Physics.Raycast (transform.position, transform.forward, out hit)) {
			distance = hit.distance;
		 } else
			distance = -1;

        Debug.Log(log+" :hit "+distance);
	}
}
