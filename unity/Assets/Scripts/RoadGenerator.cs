﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoadGenerator : MonoBehaviour
{

    [SerializeField]
    private List<GameObject> roadPrefabs;

    [SerializeField]
    private GameObject target;

    // should be > 1
    [SerializeField]
    private int numberSegments;

    private List<GameObject> generated;

    void Start()
    {
        generated = new List<GameObject>();
    }

    public void generage() {
        foreach(GameObject g in generated) {
            Destroy(g);
        }


        GameObject initial = getRandomRoad();
        var gen = Instantiate(initial, new Vector3(0, 0, 0), initial.transform.rotation);
        for (int i = 0; i < numberSegments - 1; i++)
        {
            generated.Add(gen);
            var end = gen.transform.GetChild(0);
            gen = Instantiate(getRandomRoad(), end.transform.position, end.transform.rotation);
        }

        generated.Add(gen);

        target.transform.position = gen.transform.position;
    }

	GameObject getRandomRoad(){
		return roadPrefabs[Random.Range(0,roadPrefabs.Count)];
	}
    void Update()
    {

    }
}
